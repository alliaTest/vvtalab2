package AngajatiApp.model;

import org.junit.*;

import static AngajatiApp.controller.DidacticFunction.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EmployeeTest {

    private Employee e1;
    private Employee e2;
    private Employee e3;
    private Employee e4;

    @BeforeClass
    public static void setUpAll() {
        System.out.println("Before all the Employee tests");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("Intra in Set-up si creeaza angajati");
        e1 = new Employee("Maria", "Pavlov", "2780115663399", ASISTENT, 1214.00);
        e2 = new Employee("Ionel", "Ojog", "1780115664499", ASISTENT, 850.05);
        e3 = new Employee("Paul", "Condor", "2780115443399", TEACHER, 2543.05);
        e4 = null;
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dupa test");
        e1 = null;
        e2 = null;
        e3 = null;
    }

    @Test
    public void testConstructor() {
        assertNotEquals("verificam daca s-a creat angajatul", e2, null);
    }

    @Test
    public void testConstructor2() {
        assertEquals("verificam daca s-a creat angajatul", e1.getLastName(), "Pavlov");
    }

    @Test
    public void getFirstName() {
        System.out.println("1st test: get first name");
        assertEquals("Maria", e1.getFirstName());
        assertEquals("Ionel", e2.getFirstName());
        assertEquals("Paul", e3.getFirstName());
    }

    @Test
    public void getFunction() {
        assertNotEquals(CONFERENTIAR, e1.getFunction());
        assertNotEquals(CONFERENTIAR, e2.getFunction());
        assertNotEquals(CONFERENTIAR, e3.getFunction());
    }

    @Test
    public void setSalary() {
        e3.setSalary(3500.58);
        assertEquals(3500.58, e3.getSalary(), 1);
    }

    @Ignore
    @Test(expected = NullPointerException.class)
    public void setNewEmployee() {
        e4.setFirstName("Sergiu");
        assertNotEquals("verificam daca s-a creat angajatul", e4, null);
    }

    @Test(expected = NullPointerException.class)
    public void testGetLastName2() throws Exception {
        System.out.println("Exception test");
        assertEquals("Exception", "Condor", e4.getLastName());
    }

    @Test(timeout = 20) //asteapta 2 milisecunde
    public void changeAndTime() {
        System.out.println("Timed test");
        e2.setLastName("Gorea");
        e2.setFunction(TEACHER);

        assertNotEquals("Ojog", e2.getLastName());
        assertEquals(TEACHER, e2.getFunction());
    }

    @AfterClass
    public static void tearDownAll() {
        System.out.println("After all the Employee tests");
    }
}